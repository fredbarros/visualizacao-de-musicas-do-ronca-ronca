df_ronca_ronca = data.frame()

#Define lista de Bulas do Ronca Ronca para baixar
for (i in 357:358){
  
  #Monta a URL que vai ser acessada nesta interação
  url_i <- paste(sep="","http://www.roncaronca.com.br/a-bula-do-",i)
  
  #Imprime a URL atual
  print(url_i)
  
  #Lê o conteudo da URL atual usando readLines e grava na variável "content" 
  content <- readLines(url_i)
  
  #Faz o parser nas linhas do "content" que contém as tags relacionadas as linhas que contém artista + música
  music_lines <- content[grep("<p style=\"text-align: center;\">", content)]
  
  #transforma o music_lines no dataframe df_musicas
  df_musicas <- data.frame(music_lines)
  
  #Adiciona df_musicas no dataframe df_ronca_ronca
  df_ronca_ronca <- rbind(df_ronca_ronca,df_musicas)
}

df_ronca_ronca
write.csv(df_ronca_ronca, file = "df_ronca_ronca")
